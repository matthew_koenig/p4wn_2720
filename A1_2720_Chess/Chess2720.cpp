/*
 * Chess2720.cpp
 *
 *  Created on: Sep 25, 2015
 *      Author: Matthew Koenig
 *   Purpose: Client driver for Chess game.
 */

#include <stdlib.h>
#include "Chess.h"

int main()
{
   // Intended to be start point for Chess game.
   Chess chessGame;
   chessGame.play();
   return 0;
}
