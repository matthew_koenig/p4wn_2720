/*
 * Game.h
 *
 *  Created on: Sep 25, 2015
 *      Author: Matthew Koenig
 */
#pragma once
#ifndef GAME_H_
#define GAME_H_
#include <istream>
#include <iostream>
#include "Square.h"
#include "Board.h"



class Game {
public:
  Game() : cBoard(new Board(6,6)) {}; // Constructor
   virtual ~Game() {}; // Destructor

/*
 * Provides the driving function for the game. Using algorithm
 * setup() --> Place pieces on board
 * while(isOver()){ // while game is not over
 * 		get piece location // User input to select piece to move
 * 		get piece destination // User input to select where to move piece.
 * 		movePiece (location, destination) // Move the piece.
 */
   void play();

   // Function that sets up the board for the game.
   virtual void setup() = 0;

   // Function that indicates if the game is over.
   virtual bool isOver() = 0;

   // Function that removes a piece from the Square s and places
   //the piece at Square d. Returns true if the move was successful,
   //false otherwise.
   
   bool movePiece(Square* s, Square* d);

   // Reads the input stream and returns a square from the board.
   // No prompts!
   virtual Square* getSquare(std::istream &is) = 0;

   // Implimentation of Board in class game inherited by class Chess
   // Needed here for redraws after moving a piece.
   Board* cBoard;
};

#endif
