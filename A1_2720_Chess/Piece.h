/*
 * Piece.h
 *
 *  Created on: Sep 26, 2015
 *      Author: Matthew Koenig
 */																
#pragma once
#ifndef PIECE_H_
#define PIECE_H_

#include <stdlib.h>
#include <iostream>

class Piece{
  private:
   /*
    * Attributes definining the team and the type of piece
    *		colour (1 = black, 0 = white)
    *		symbol (Upercase = black pieces, Lowercase = white pieces)
    *			r = rook
    *			b = bishop
    *			k = king
    *			q = queen
    *			p = pawn
    */


   /*
    * Returns true if the piece is still alive
    */
   bool isAlive();

   /*
    * Sets the piece to be inactive on the board.
    */
   void kill();

  public:
   Piece();
   Piece(int colour, char symbol);
   ~Piece();

   int colour;
   char symbol;
   bool active;
};
#endif
