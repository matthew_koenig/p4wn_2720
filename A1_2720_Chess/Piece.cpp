/*
 * Piece.cpp
 *
 *  Created on: Sep 26, 2015
 *      Author: Matthew Koenig
 */
#include <stdlib.h>
#include "Piece.h"

Piece::Piece()
{
   bool active = true;
}

// Initialize the pieces
Piece::Piece(int pColour, char pSymbol)
{
   if(pSymbol != NULL && pColour == 0 || pColour == 1)
   {
      colour = pColour;
      symbol = pSymbol;
      active = true;
   }
   else
   {
      colour = NULL;
      symbol = NULL;
      active = false;
   }
}

Piece::~Piece()
{
}

// returns true if the piece is still active on the board, false otherwise.
bool Piece::isAlive()
{
   if (active == true)
   {
      return true;
   }
   else
   {
      return false;
   }
}
// sets the piece to be inactive on the board.
void Piece::kill()
{
   colour = NULL;
   symbol = NULL;
   active = false;
}
