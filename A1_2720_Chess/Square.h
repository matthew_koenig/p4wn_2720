/*
 * Square.h
 *
 *  Created on: Sep 26, 2015
 *      Author: Matthew Koenig
 */
#pragma once
#ifndef SQUARE_H_
#define SQUARE_H_

#include <stdlib.h>
#include <iostream>
#include "Piece.h"


class Square{
  public:

   Square();
   /*
    * Default constructor: Takes two parameters (row and column)
    *	int row: Row of board 0 = top 5 = bottom.
    *	int column: Column of board 0 = left 5 = right.
    *	Default row column starts at 0, 0
    */
   Square(int row, int column);

   /*
    * Returns the symbol of the piece on the square, or a
    * symbol indicating an empty square (".")
    */

   ~Square();

   // returns the symbol of the piece on the square,
   // or a symbol indicating an empty square (“.”).
   char symbol();

   /*
    * A function that sets a piece on the square
    */
   void setPiece(Piece* p);

   /*
    * A function that returns the piece on the square,
    * it it exists. Otherwise returns NULL
    */
   Piece* getPiece();

   /*
    * A function that removes a piece from a square and returns it,
    * if it exists otherwise returns NULL.
    */
   Piece* removePiece();

   int i_sqRow;
   int i_sqCol;

   Piece * cPiece;

  private:
};
#endif
