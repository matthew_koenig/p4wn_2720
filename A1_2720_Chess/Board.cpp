/*
 * Board.cpp
 *
 *  Created on: Sep 26, 2015
 *      Author: Matthew Koenig
 */

#include <stdlib.h>
#include <iostream>
#include "Board.h"
#include "Square.h"
#include <assert.h>

// An abstraction of a two-dimensional board. The board is made of squares

Board::Board()
{
   Board(6,6);
}

Board::Board(int width, int height)
{
   // Create all the squares
   for (int iRow = 0; iRow < height; iRow++)
   {
      for (int iCol = 0; iCol < width; iCol++)
      {
	 cSquares[iRow][iCol] = new Square(iRow, iCol);
      }
   }
};

// Destructor intended to delete all the squares
Board::~Board()
{
   for (int iRow = 0; iRow < 6; iRow++)
   {
      for (int iCol = 0; iCol < 6; iCol++)
      {
	 delete cSquares[iRow][iCol];
      }
   }
}


// a function that draws the board to an output stream.
void Board::draw(std::ostream& o)
{
   std::system("clear");
   char space = ' ';

   std::cout << "  " << "0" << space << "1" << space << "2" << space << "3" << space << "4" << space << "5" << "\n";
   for (int iRow = 0; iRow < 6; iRow++)
   {
      std::cout << iRow << space;
      for (int iCol = 0; iCol < 6; iCol++)
      {
	 Square * s = this->getSquare(iRow, iCol);
	 std::cout << s->symbol() << space;
      }
      std::cout << std::endl;
   }
}

// a function that places a Piece on a Square. 
void Board::placePiece(Piece* p, Square* s)
{
   int row = s-> i_sqRow;
   int col = s-> i_sqCol;

   cSquares[row][col]->cPiece = p;
}

// a function that moves the Piece on Square s 
void Board::movePiece(Square* s, Square* d)
{
// Ensure there is an active piece on the source square
   assert(s->cPiece->active == true);

   d->cPiece = s->cPiece;
   s->cPiece = NULL;
}

// a function that returns the Square at row r and column
Square* Board::getSquare(int r, int c)
{
   return cSquares[r][c];
}
