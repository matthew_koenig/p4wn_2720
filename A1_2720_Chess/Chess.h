/*
 * Chess.h
 *
 *  Created on: Sep 25, 2015
 *      Author: Matthew Koenig
 */																
#pragma once
#ifndef CHESS_H_
#define CHESS_H_
#include "Game.h"
#include "Board.h"


class Chess : public Game{
  public:

   //Creates a 6x6 chess board.
   Chess()
   {

   };
   ~Chess();

   
   // Overrides setup in Game places pieces on board.

   void setup();


   //Function to indicate that the game ends when one of the
   //King pieces is taken.

   bool isOver();

   /*
    * Overrides the getSquare() to read the row and column for a location on
    * the board. The row and column values are seperated by a space on one
    * line. Use assert() to verify that the coordinates are valid for the
    * board.
    */
   Square* getSquare(std::istream &is);

};
#endif
