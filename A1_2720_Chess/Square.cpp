/*
 * Square.cpp
 *
 *  Created on: Sep 26, 2015
 *      Author: Matthew Koenig
 */
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "Square.h"
#include "Piece.h"

Square::Square()
{
}

//  for specifying the location of the square on the board.
Square::Square(int row, int column)
{
   cPiece = NULL;
   i_sqRow = row;
   i_sqCol = column;
}

// Destructor for Square removes cPiece
Square::~Square()
{
   delete cPiece;
}

// returns the symbol of the piece on the square,
// or a symbol indicating an empty square (“.”).
char Square::symbol()
{

   Piece* p = getPiece();
   if (p != NULL)
   {
      return p->symbol;
   }
   else
   {
      return '.';
   }
}

void Square::setPiece(Piece* p)
{
   // Assign p to the private piece of this square.
   this->cPiece = p;
}

// a function that sets a piece on the square.
Piece* Square::getPiece()
{
   // Return this piece if it exists otherwise return NULL.
   if (this->cPiece != NULL)

   {
      return this->cPiece;
   }
   else
   {
      return NULL;
   }
}

// a function that removes a piece from a square and returns it,
// if it exists, otherwise returns NULL;
Piece* Square::removePiece()
{
   // If this piece exists return it otherwise return NULL
   // This is confusing "A function that removes a piece from a
   //square and returns it, if it exists, otherwise returns NULL;
   if (this->cPiece != NULL)
   {
      Piece* tempPiece = new Piece;
      tempPiece = this->cPiece;
      this->cPiece = NULL;
      return tempPiece;
   }
   return NULL;
}

