/*
 * Board.h
 *
 *  Created on: Sep 26, 2015
 *      Author: Matthew Koenig
 */

#pragma once
#ifndef BOARD_H_
#define BOARD_H_

#include <stdlib.h>
#include <iostream>
#include "Square.h"

class Board{
  public:
   /*
    * Constructor:
    *	Specifies the size of the board
    *	Takes two parameters,
    *		int width (default 6)
    *		int height (default 6)
    */
   Board(); // Default constructor (6 x 6)
   Board(int width, int height); // Variable size constructor
   ~Board();

   /*
    * A function that draws the board to an output stream.
    */
   void draw(std::ostream& o);

   /*
    * A function that places a piece on a square
    */
   void placePiece(Piece * p, Square* s);

   /*
    * A function that moves the piece on Square s to
    * Square d. Uses assert() to verify that a piece exists on s.
    */
   void movePiece(Square* s, Square* d);

   /*
    * A function that returns the Square at row r and column c
    */
   Square* getSquare(int r, int c);

   // An array of squares for the board.
   Square * cSquares[6][6];
	
};
#endif
