/*
 * Chess.cpp
 *
 *  Created on: Sep 25, 2015
 *      Author: Matthew Koenig
 */
#include "Game.h"
#include "Chess.h"
#include <iostream>
#include <string>
#include <assert.h>

Chess::~Chess()
{
   delete cBoard;
}

/**
* Overrides setup in Game places pieces on board.
*/
void Chess::setup()
{


   // Set white Pieces

   // White Rook
   Piece* pieceWhiteRookLeft = new Piece(0,'r');
   cBoard->cSquares[0][0]->setPiece(pieceWhiteRookLeft);

   // White Bishop
   Piece* pieceWhiteBishopLeft = new Piece(0,'b');
   cBoard->cSquares[0][1]->setPiece(pieceWhiteBishopLeft);

   // White King
   Piece* pieceWhiteKing = new Piece(0,'k');
   cBoard->cSquares[0][2]->setPiece(pieceWhiteKing);

   // White Queen
   Piece* pieceWhiteQueen = new Piece(0,'q');
   cBoard->cSquares[0][3]->setPiece(pieceWhiteQueen);

   // White Bishop
   Piece* pieceWhiteBishopRight = new Piece(0,'b');
   cBoard->cSquares[0][4]->setPiece(pieceWhiteBishopRight);

   // White Rook
   Piece* pieceWhiteRookRight = new Piece(0,'r');
   cBoard->cSquares[0][5]->setPiece(pieceWhiteRookRight);

   // White Pawn
   Piece* pieceWhitePawn1 = new Piece(0,'p');
   cBoard->cSquares[1][0]->setPiece(pieceWhitePawn1);

   // White Pawn
   Piece* pieceWhitePawn2 = new Piece(0,'p');
   cBoard->cSquares[1][1]->setPiece(pieceWhitePawn2);

   // White Pawn
   Piece* pieceWhitePawn3 = new Piece(0,'p');
   cBoard->cSquares[1][2]->setPiece(pieceWhitePawn3);

   // White Pawn
   Piece* pieceWhitePawn4 = new Piece(0,'p');
   cBoard->cSquares[1][3]->setPiece(pieceWhitePawn4);

   // White Pawn
   Piece* pieceWhitePawn5 = new Piece(0,'p');
   cBoard->cSquares[1][4]->setPiece(pieceWhitePawn5);

   // White Pawn
   Piece* pieceWhitePawn6 = new Piece(0,'p');
   cBoard->cSquares[1][5]->setPiece(pieceWhitePawn6);
   // Set Black Pieces -------------------------------------

   // Black Rook
   Piece* pieceBlackRookLeft = new Piece(1,'R');
   cBoard->cSquares[5][0]->setPiece(pieceBlackRookLeft);

   // Black Bishop
   Piece* pieceBlackBishopLeft = new Piece(1,'B');
   cBoard->cSquares[5][1]->setPiece(pieceBlackBishopLeft);

   // Black King
   Piece* pieceBlackKing = new Piece(1,'K');
   cBoard->cSquares[5][2]->setPiece(pieceBlackKing);

   // Black Queen
   Piece* pieceBlackQueen = new Piece(1,'Q');
   cBoard->cSquares[5][3]->setPiece(pieceBlackQueen);

   // Black Bishop
   Piece* pieceBlackBishopRight = new Piece(1,'B');
   cBoard->cSquares[5][4]->setPiece(pieceBlackBishopRight);

   // Black Rook
   Piece* pieceBlackRookRight = new Piece(1,'R');
   cBoard->cSquares[5][5]->setPiece(pieceBlackRookRight);

   // Black Pawn
   Piece* pieceBlackPawn1 = new Piece(1,'P');
   cBoard->cSquares[4][0]->setPiece(pieceBlackPawn1);

   // Black Pawn
   Piece* pieceBlackPawn2 = new Piece(1,'P');
   cBoard->cSquares[4][1]->setPiece(pieceBlackPawn2);

   // Black Pawn
   Piece* pieceBlackPawn3 = new Piece(1,'P');
   cBoard->cSquares[4][2]->setPiece(pieceBlackPawn3);

   // Black Pawn
   Piece* pieceBlackPawn4 = new Piece(1,'P');
   cBoard->cSquares[4][3]->setPiece(pieceBlackPawn4);

   // Black Pawn
   Piece* pieceBlackPawn5 = new Piece(1,'P');
   cBoard->cSquares[4][4]->setPiece(pieceBlackPawn5);

   // Black Pawn
   Piece* pieceBlackPawn6 = new Piece(1,'P');
   cBoard->cSquares[4][5]->setPiece(pieceBlackPawn6);

   // Draw the board
   cBoard->draw(std::cout);
}

/**
* Function to indicate that the game ends when one of the King pieces is taken.
* Overrides isOver in game.
*/
bool Chess::isOver()
{
   // numOfKings used to count the Kings remaining on the board
   // there is probably a better way to determine that a king is dead
   // but that was the intent.
   
   int numOfKings = 0;
   // Loop through pieces until you find the kings.
   for(int iRow = 0; iRow < 6; iRow++)
   {
      for (int iCol = 0; iCol < 6; iCol++)
      {
	 // Initialize testPiece and testSquare to hold the instance of the
	 // object you wish to test for king being alive.
	 Square* testSquare = cBoard->cSquares[iRow][iCol];
	 Piece* testPiece = testSquare->getPiece();

	 // Logic test to ensure that testPiece is not NULL
	 // Used to avoid null ref
	 if (testPiece != NULL)
	 {
	    // If the testPiece symbol is a k or a K
	    // That is all we care about
	    if (testPiece->symbol == 'k' || testPiece->symbol == 'K')
	    {
	       // For each king increment the numOfKings.
	       numOfKings++;
	       // And the test Piece is not active
	       if (testPiece->active == false)
	       {
		  // Return True (The game "isOver")
		  return true;
	       }
	    }
	 }
      }
   }
   if (numOfKings == 2)
   {
      // If no King is marked inactive return false (the game "isNotOver")
      return false;
   }
   else
   {
      // There are less than two kings so the game "isOver"
      return true;
   }
}


/** 
* Function to read the row and column for a location on the baord. 
* The row and column values are seperated by a space on one line. 
* User assert() to verify that the coordinates are valid for the board.
*/

Square* Chess::getSquare(std::istream &is)
{
   int iRow;
   int iCol;

   is >> iRow >> iCol;

   assert(iRow < 6 && iCol < 6);

   return cBoard->getSquare(iRow, iCol);
}
