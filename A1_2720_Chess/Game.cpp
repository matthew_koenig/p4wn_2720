/*
 * Game.cpp
 *
 *  Created on: Sep 25, 2015
 *      Author: Matthew Koenig
 *   Purpose: Base abstract class for Chess class
 */
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include "Game.h"
#include "Chess.h"

/*
* Provides the driving function for the game. Using algorithm
* setup() --> Place pieces on board
* while(isOver()){ // while game is not over
* 		get piece location // User input to select piece to move
* 		get piece destination // User input to select where to move piece.
* 		movePiece (location, destination) // Move the piece.
*/
void Game::play()
{
   // Driving loop for game used to setup the board initialy and to
   // provide re-draws when pieces move.
   
   setup();
   while(!isOver())
   {
      Square * SourceSquare = NULL;
      Square * DestSquare = NULL;

      // Get the square requested by the client so that the
      // piece can be moved from the source square to the
      // destination square.
      
      SourceSquare = getSquare(std::cin);
      DestSquare = getSquare(std::cin);
      movePiece(SourceSquare, DestSquare);

      cBoard->draw(std::cout);
      isOver();
   }
}

// Move the piece from the source square to the destination square.

bool Game::movePiece(Square* s, Square* d)
{
   assert(s->cPiece->active == true);

   d->cPiece = s->cPiece;
   s->cPiece = NULL;
   return true;
}




